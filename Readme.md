# vamp198e - Vagrant Lamp - Ubuntu Apache Mysql PHP Python Django Stack
_____________


Codename: vamp198e

This runs the code from our server at PMDS.

## How this can be used:

This creates a vagrant ubuntu 14.04 server in virtualbox with all the server packages.  
Website code is located in c:\p2\vamp\htdocs - you put it there.  
mysql data is imported, user created.
Settings are all there to run the php code and the django code. Just visit the menu.  


- In Windows
- Install vagrant
- Install virtualbox  - can't remember isn't already installed by vagrant.
- Put this repo in c:\data\vagrantyard\vamp198e or any other folder of your choice, I think.
- copy `....\vagrantyard\vamp198e\home\vagrant\configv1-example.sh` to `configv1.sh` and edit the passwords.
- Put code the from our server {from c:\p2\xampp\htdocs} in `c:\p2\vamp\htdocs`
- open cmd prompt in above folder.
- vagrant up
- get a coffee, after about 6-15 minutes...
- visit `192.168.3.6/menu` in your browser


## Confidential

This repo is **NOT** confidential.  
The mysql data and the server passwords - **IS** confidential.  
The resulting VM is confidential. Don't share the data, the mysql database, passwords in the website code, etc.  



#### David Gleba  
[https://github.com/dgleba](https://github.com/dgleba)  
[https://bitbucket.org/dgleba/vamp198e](https://bitbucket.org/dgleba/vamp198e)  
dgleba@gmail.com   
2015-09-30  
  

_____________